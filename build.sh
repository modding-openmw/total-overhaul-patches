#!/bin/sh
set -e

cd "01 Redaynia Restored+Mushroom Tree Replacer Patch"
delta_plugin convert "Redaynia Restored Mushroom Tree Replacer Patch.yaml"
mv "Redaynia Restored Mushroom Tree Replacer Patch.omwaddon" "Redaynia Restored Mushroom Tree Replacer Patch.esp"
cd ..

cd "02 BCOM+Imperial Towns Revamp Patch"
delta_plugin convert "BCOM+Imperial Towns Revamp Patch.yaml"
mv "BCOM+Imperial Towns Revamp Patch.omwaddon" "BCOM+Imperial Towns Revamp Patch.esp"
cd ..

cd "03 UvirithsLegacy+TR Patch"
delta_plugin convert "UvirithsLegacy+TR Patch.yaml"
mv "UvirithsLegacy+TR Patch.omwaddon" "UvirithsLegacy+TR Patch.esp"
cd ..

# cd "04 Kogoruhn - Extinct City of Ash and Sulfur+ROHT Patch"
# delta_plugin convert "Kogoruhn - Extinct City of Ash and Sulfur+ROHT Patch.yaml"
# mv "Kogoruhn - Extinct City of Ash and Sulfur+ROHT Patch.omwaddon" "Kogoruhn - Extinct City of Ash and Sulfur+ROHT Patch.esp"
# cd ..

cd "05 BCOM+GhastlyGG+DynamicDistantBuildingsPatch/BCOM+GhastlyGG+DynamicDistantBuildingsPatch.d"
delta_plugin convert BCOM+GhastlyGG+DynamicDistantBuildingsPatch.yaml
mv BCOM+GhastlyGG+DynamicDistantBuildingsPatch.omwaddon ../BCOM+GhastlyGG+DynamicDistantBuildingsPatch.esp
cd ../..

cd "06 OAABShipwrecks+RRBetterShipsnBoatsPatch"
delta_plugin convert OAABShipwrecks+RRBetterShipsnBoatsPatch.yaml
mv OAABShipwrecks+RRBetterShipsnBoatsPatch.omwaddon OAABShipwrecks+RRBetterShipsnBoatsPatch.esp
delta_plugin convert JFK+OAABShipwrecks+RRBetterShipsnBoatsPatch.yaml
mv JFK+OAABShipwrecks+RRBetterShipsnBoatsPatch.omwaddon JFK+OAABShipwrecks+RRBetterShipsnBoatsPatch.esp
cd ..

cd "07 NordicDagonFel+RRBetterShipsnBoatsPatch"
delta_plugin convert NordicDagonFel+RRBetterShipsnBoatsPatch.yaml
mv NordicDagonFel+RRBetterShipsnBoatsPatch.omwaddon NordicDagonFel+RRBetterShipsnBoatsPatch.esp
cd ..

cd "08 BCOM+TOTSP+RRBetterShipsnBoatsPatch"
delta_plugin convert BCOM+TOTSP+RRBetterShipsnBoatsPatch.yaml
mv BCOM+TOTSP+RRBetterShipsnBoatsPatch.omwaddon BCOM+TOTSP+RRBetterShipsnBoatsPatch.esp
cd ..

cd "09 RRBetterShipsnBoatsChargenPatch"
delta_plugin convert RRBetterShipsnBoatsChargenPatch.yaml
mv RRBetterShipsnBoatsChargenPatch.omwaddon RRBetterShipsnBoatsChargenPatch.esp
cd ..
