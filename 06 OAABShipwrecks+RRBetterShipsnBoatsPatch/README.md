#### 06 OAABShipwrecks+RRBetterShipsnBoats

##### About

Provide compatibility between [RR Mod Series - Better Ships and Boats](https://www.nexusmods.com/morrowind/mods/44001) and [OAAB Shipwrecks](https://www.nexusmods.com/morrowind/mods/51364) as well as a second plugin that supports [OAAB Shipwrecks and Justice for Khartag - merged plugin](https://www.nexusmods.com/morrowind/mods/53044).

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Probably
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/Landmasses/TamrielData/HD"
...
data="/home/username/games/openmw/Mods/ModdingResources/OAAB_Data/00 Core"
...
data="/home/username/games/openmw/Mods/ObjectsClutter/RRModSeriesBetterShipsandBoats/00 - Main Files"
data="/home/username/games/openmw/Mods/ObjectsClutter/RRModSeriesBetterShipsandBoats/01 - Main ESP - English"
# OR, not both!
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/patches/26 RR Ships and Boats"
...
data="/home/username/games/openmw/Mods/FloraLandscape/OAABShipwrecks/00 Core"
data="/home/username/games/openmw/Mods/Patches/OAABShipwrecksandJusticeforKhartagmergedplugin"
data="/home/username/games/openmw/Mods/Patches/TotalOverhaulPatches/06 OAABShipwrecks+RRBetterShipsnBoatsPatch"
...
content=Tamriel_Data.esm
...
content=OAAB_Data.esm
...
# Use with regular OAAB Shipwrecks
content=OAAB - Shipwrecks.ESP
...
# Or with OAAB Shipwrecks and Justice for Khartag - merged plugin
content=Justice4Khartag+OAAB_Shipwreck - merged.ESP
...
content=RR_Better_Ships_n_Boats_Eng.esp
# Use this with regular OAAB Shipwrecks
content=OAABShipwrecks+RRBetterShipsnBoatsPatch.esp
# Or this with OAAB Shipwrecks and Justice for Khartag - merged plugin
content=JFK+OAABShipwrecks+RRBetterShipsnBoatsPatch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/total-overhaul-patches/-/tree/master/06%20OAABShipwrecks%2BRRBetterShipsnBoatsPatch)
* [Patch Home](https://modding-openmw.gitlab.io/total-overhaul-patches/)
* [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)
