#### 09 RRBetterShipsnBoatsChargenPatch

##### About

The guard standing outside on the deck of the boar during chargen is floating when you use [RR Mod Series - Better Ships and Boats](https://www.nexusmods.com/morrowind/mods/44001). This patch moves him back and to the side a bit so that he's level with the deck.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Probably
* `Rebirth`: No

##### Load Order

```
...
content=RR_Better_Ships_n_Boats_Eng.esp
content=RRBetterShipsnBoatsChargenPatch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/total-overhaul-patches/-/tree/master/09%20RRBetterShipsnBoatsChargenPatch)
* [Patch Home](https://modding-openmw.gitlab.io/total-overhaul-patches/)
* [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)
