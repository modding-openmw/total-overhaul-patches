# Total Overhaul Patches

Patches to fix things for [the Total Overhaul mod list](https://modding-openmw.com/lists/total-overhaul/) on [Modding-OpenMW.com](https://modding-openmw.com/).

#### Credits

**Author**: johnnyhostile

**Special Thanks**:

* Benjamin Winger for making [Delta Plugin](https://gitlab.com/bmwinger/delta-plugin/)
* Corsair83 for making [OAAB Shipwrecks](https://www.nexusmods.com/morrowind/mods/51364)
* Demanufacturer87 for making [Kogoruhn - Extinct City of Ash and Sulfur](https://www.nexusmods.com/morrowind/mods/51615)
* Hemaris for making [Dynamic Distant Buildings for OpenMW](https://www.nexusmods.com/morrowind/mods/51236)
* mort, Pozzo, Karpik777, and bhl for their work on [Rise of House Telvanni](https://www.nexusmods.com/morrowind/mods/27545) ([2.0](https://www.nexusmods.com/morrowind/mods/48225))
* RandomPal for making [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231) and [Nordic Dagon Fel](https://www.nexusmods.com/morrowind/mods/49603)
* Reizeron for making [Redaynia Restored](https://www.nexusmods.com/morrowind/mods/47646)
* Resdayn Revival Team for making [RR Mod Series - Better Ships and Boats](https://www.nexusmods.com/morrowind/mods/44001)
* Stuporstar for making [Uvirith's Legacy](https://stuporstar.sarahdimento.com/)
* SVNR for making [Imperial Towns Revamp](https://www.nexusmods.com/morrowind/mods/49735)
* Tapetenklaus for making [Dr_Data](https://www.nexusmods.com/morrowind/mods/51776)
* wazabear for making [Ghastly Glowyfence](https://www.nexusmods.com/morrowind/mods/47982)
* The TOTSP Team for making [Solstheim Tomb of the Snow Prince](https://www.nexusmods.com/morrowind/mods/46810)
* The Tamriel Rebuilt Team for making [Tamriel Rebuilt](https://www.tamriel-rebuilt.org/)
* The OpenMW team, including every contributor (for making OpenMW and OpenMW-CS)
* All the users in the `modding-openmw-dot-com` Discord channel on the OpenMW server for their dilligent testing <3
* Bethesda for making Morrowind

And a big thanks to the entire OpenMW and Morrowind modding communities! I wouldn't be doing this without all of you.

#### Web

[Project Home](https://modding-openmw.gitlab.io/total-overhaul-patches/)

[Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)

[Source on GitLab](https://gitlab.com/modding-openmw/total-overhaul-patches)

#### Installation

1. Download the zip from a link above.
1. **Note that included images for `Kogoruhn - Extinct City of Ash and Sulfur+ROHT Patch` and `BCOM+GhastlyGG+DynamicDistantBuildingsPatch` contain plot/quest spoilers!**
1. Extract the contents to a location of your choosing.
1. See each mod's individual `README.md` file for detailed load order information.
1. Note that these plugins won't be of any use without their dependent plugins, and these need to load after them. Enjoy!

#### Connect

* [Discord](https://discord.gg/KYKEUxFUsZ)
* [IRC](https://web.libera.chat/?channels=#momw)
* File an issue [on GitLab](https://gitlab.com/modding-openmw/total-overhaul-patches/-/issues/new) for bug reports or feature requests
* Email: `johnnyhostile at modding-openmw dot com`
* Leave a comment [on Nexus mods](https://www.nexusmods.com/morrowind/mods/52989?tab=posts)
