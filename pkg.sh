#!/bin/sh
set -eu

file_name=total-overhaul-patches.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

zip --must-match --recurse-paths \
    $file_name \
    "01 Redaynia Restored+Mushroom Tree Replacer Patch" \
    "02 BCOM+Imperial Towns Revamp Patch" \
    "03 UvirithsLegacy+TR Patch" \
    "05 BCOM+GhastlyGG+DynamicDistantBuildingsPatch" \
    "06 OAABShipwrecks+RRBetterShipsnBoatsPatch" \
    "07 NordicDagonFel+RRBetterShipsnBoatsPatch" \
    "08 BCOM+TOTSP+RRBetterShipsnBoatsPatch" \
    "09 RRBetterShipsnBoatsChargenPatch" \
    CHANGELOG.md \
    LICENSE \
    README.md \
    version.txt \
    --exclude \*.yaml \
    --exclude ./05\ BCOM+GhastlyGG+DynamicDistantBuildingsPatch/BCOM+GhastlyGG+DynamicDistantBuildingsPatch.d\*
sha256sum $file_name > $file_name.sha256sum.txt
sha512sum $file_name > $file_name.sha512sum.txt
