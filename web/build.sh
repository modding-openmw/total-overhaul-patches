#!/bin/sh
set -eu
#
# This script builds the project website. It downloads soupault as needed and
# then runs it, the built website can be found at: web/build
#

this_dir=$(realpath "$(dirname "${0}")")
cd "${this_dir}"

soupault_version=4.5.0
soupault_pkg=soupault-${soupault_version}-linux-x86_64.tar.gz
soupault_path=./soupault-${soupault_version}-linux-x86_64

shas=https://github.com/PataphysicalSociety/soupault/releases/download/${soupault_version}/sha256sums
spdl=https://github.com/PataphysicalSociety/soupault/releases/download/${soupault_version}/${soupault_pkg}

if ! [ -f ${soupault_path}/soupault ]; then
    wget ${shas}
    wget ${spdl}
    tar xf ${soupault_pkg}
    grep linux sha256sums | sha256sum -c -
fi

cp ../CHANGELOG.md site/changelog.md
cp ../README.md site/readme.md

{
    cat ../01\ Redaynia\ Restored+Mushroom\ Tree\ Replacer\ Patch/README.md
    cat ../02\ BCOM+Imperial\ Towns\ Revamp\ Patch/README.md
    cat ../03\ UvirithsLegacy+TR\ Patch/README.md
    cat ../04\ Kogoruhn\ -\ Extinct\ City\ of\ Ash\ and\ Sulfur+ROHT\ Patch/README.md
    cat ../05\ BCOM+GhastlyGG+DynamicDistantBuildingsPatch/README.md
    cat ../06\ OAABShipwrecks+RRBetterShipsnBoatsPatch/README.md
    cat ../07\ NordicDagonFel+RRBetterShipsnBoatsPatch/README.md
    cat ../08\ BCOM+TOTSP+RRBetterShipsnBoatsPatch/README.md
    cat ../09\ RRBetterShipsnBoatsChargenPatch/README.md
} | grep -Ev "Nexus|Patch Home" >> site/readme.md

cp ../01\ Redaynia\ Restored+Mushroom\ Tree\ Replacer\ Patch/Redaynia\ Restored\ Mushroom\ Tree\ Replacer\ Patch.png site/img/
cp ../02\ BCOM+Imperial\ Towns\ Revamp\ Patch/BCOM+Imperial\ Towns\ Revamp\ Patch.png site/img/
cp ../03\ UvirithsLegacy+TR\ Patch/UvirithsLegacy+TR\ Patch.png site/img/
cp ../05\ BCOM+GhastlyGG+DynamicDistantBuildingsPatch/BCOM+GhastlyGG+DynamicDistantBuildingsPatch1.png site/img/

PATH=${soupault_path}:$PATH soupault "$@"
