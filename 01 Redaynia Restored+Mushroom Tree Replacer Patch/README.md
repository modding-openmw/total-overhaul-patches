#### 01 Redaynia Restored+Mushroom Tree Replacer Patch

##### About

Fixes the placement of a mushroom tree for Redaynia Restored by Reizeron and Mushroom Tree Replacer by PeterBitt. Also deletes a floating sack.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Probably
* `Rebirth`: No

##### Load Order

```
...
data="/home/username/games/openmw/Mods/CitiesTowns/RedayniaRestored/Data Files"
data="/home/username/games/openmw/Mods/Patches/TotalOverhaulPatches/01 Redaynia Restored+Mushroom Tree Replacer Patch"
...
content=Redaynia Restored.ESP
content=Redaynia Restored Mushroom Tree Replacer Patch.esp
...
```

##### Web

* [Source on GitLab](https://gitlab.com/modding-openmw/total-overhaul-patches/-/tree/master/01%20Redaynia%20Restored%2BMushroom%20Tree%20Replacer%20Patch)
* [Patch Home](https://modding-openmw.gitlab.io/total-overhaul-patches/)
* [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)
