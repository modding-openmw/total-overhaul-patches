## Total Overhaul Patches Changelog

#### 2.7

* Added `09 RRBetterShipsnBoatsChargenPatch` to fix a floating chargen guard with [RR Mod Series - Better Ships and Boats](https://www.nexusmods.com/morrowind/mods/44001).
* Added `08 BCOM+TOTSP+RRBetterShipsnBoatsPatch` to provide compatibility between [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231), [RR Mod Series - Better Ships and Boats](https://www.nexusmods.com/morrowind/mods/44001), and [Solstheim Tomb of the Snow Prince](https://www.nexusmods.com/morrowind/mods/46810).

<!--[Download Link](https://gitlab.com/modding-openmw/total-overhaul-patches/-/packages/TODO) | --> [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)

#### 2.6

* Temporarily removed `04 Kogoruhn - Extinct City of Ash and Sulfur+ROHT Patch` as it is not compatible with the latest version of the upstream mod.
* Updated `06 OAABShipwrecks+RRBetterShipsnBoatsPatch` to include a second patch that supports [OAAB Shipwrecks and Justice for Khartag - merged plugin](https://www.nexusmods.com/morrowind/mods/53044).

[Download Link](https://gitlab.com/modding-openmw/total-overhaul-patches/-/packages/15913205) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)

#### 2.5

* Clarified the usage and compatibility of `UvirithsLegacy+TR Patch`; it likely won't work with Morrowind.exe and is made for a very specific version of the UL+TR patch.
* Added `07 NordicDagonFel+RRBetterShipsnBoatsPatch` which provides compatibility between [RR Mod Series - Better Ships and Boats](https://www.nexusmods.com/morrowind/mods/44001) and [Nordic Dagon Fel](https://www.nexusmods.com/morrowind/mods/49603).
* Added `06 OAABShipwrecks+RRBetterShipsnBoatsPatch` which provides compatibility between [RR Mod Series - Better Ships and Boats](https://www.nexusmods.com/morrowind/mods/44001) and [OAAB Shipwrecks](https://www.nexusmods.com/morrowind/mods/51364).

[Download Link](https://gitlab.com/modding-openmw/total-overhaul-patches/-/packages/15559013) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)

#### 2.4

* Reimplemented `BCOM+Dynamic Distant Buildings` as a direct edit of [Dynamic Distant Buildings for OpenMW](https://www.nexusmods.com/morrowind/mods/51236) rather than a patch on top of it due to various issues that emerged with the patch approach.
* Reimplemented `UvirithsLegacy+TR Patch` without deletes which should improve compatibility.

[Download Link](https://gitlab.com/modding-openmw/total-overhaul-patches/-/packages/15188036) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)

#### 2.3

* Updated `BCOM+Imperial Towns Revamp Patch` to support BCOM 2.9.4 which contains a change that gives the wheel moved by the patch a unique ID. This means the patch can be more future proof and won't need to be updated for every BCOM release. Huge thanks to RandomPal for doing that!

[Download Link](https://gitlab.com/modding-openmw/total-overhaul-patches/-/packages/15066976) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)

#### 2.2

* Renamed all `.omwaddon` plugins to `.esp` so that they may better work with tooling such as [Mlox](https://github.com/rfuzzo/mlox).
* Updated `BCOM+Imperial Towns Revamp Patch` to support BCOM 2.9.3.
* Improved and renamed the BCOM+Dynamic Distant Buildings plugin; it now lists Ghastly Glowyfence as a master and should be less suseptible to problems caused by deleting things since it was reimplemented without most deletes.
* `Riharradroon - Path to Kogoruhn v1.0.ESP` is now a master for `04 Kogoruhn - Extinct City of Ash and Sulfur+ROHT Patch`, and the masters are all now sorted correctly.

[Download Link](https://gitlab.com/modding-openmw/total-overhaul-patches/-/packages/15065174) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)

#### 2.1

* Fixed some typos.

[Download Link](https://gitlab.com/modding-openmw/total-overhaul-patches/-/packages/14965301) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)

#### 2.0

* Each patch has their own small README that gets appended to the `/readme/` page on the website.
* Renamed `01 Redaynia Restored Patch` to `01 Redaynia Restored+Mushroom Tree Replacer Patch` for more consistency with other option namings.
* Added a patch that fixes compatibility between [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231) and [Dynamic Distant Buildings for OpenMW](https://www.nexusmods.com/morrowind/mods/51236)'s Ghostgate module.
* Added a patch that fixes compatibility between [Kogoruhn - Extinct City of Ash and Sulfur](https://www.nexusmods.com/morrowind/mods/51615) and [Rise of House Telvanni](https://www.nexusmods.com/morrowind/mods/27545) by disabling various objects when a certain plot point has been reached (the provided image contains spoilers!)
* Added a patch to fix compatibility between [Uvirith's Legacy](https://stuporstar.sarahdimento.com/) and [Tamriel Rebuilt](https://www.tamriel-rebuilt.org/) by deleting an old teleport platform that went to a cell that's since been removed. Additionally, a few walls were deleted to make paths to teleport pads, and a few platforms that went to now-nonexistent cells have been deleted as well.

[Download Link](https://gitlab.com/modding-openmw/total-overhaul-patches/-/packages/14965209) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)

#### 1.0

* Initial release of the mod.

[Download Link](https://gitlab.com/modding-openmw/total-overhaul-patches/-/packages/14912246) | [Nexus Mods](https://www.nexusmods.com/morrowind/mods/52989)
